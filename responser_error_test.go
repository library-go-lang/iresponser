package iresponser_test

import (
	"gitlab.com/library-go-lang/apperror"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/iresponser"
)

func TestNewResponserError(t *testing.T) {
	err := apperror.ErrDataNotFound

	resp := iresponser.NewResponserError(err)

	assert.Error(t, resp.Error(), "[TestNewResponserError] Should error")
	assert.Nil(t, resp.Data(), "[TestNewResponserError] Data should nil")
	assert.Nil(t, resp.Meta(), "[TestNewResponserError] Meta should nil")
	assert.Equal(t, "Data is not found", resp.Message(), "[TestNewResponserError] Message should \"Data is not found\"")
	assert.Equal(t, http.StatusNotFound, resp.HTTPStatus(), "[TestNewResponserError] HTTP Status should 404")
	assert.Equal(t, "err_not_found", resp.Code(), "[TestNewResponserError] Code should \"err_not_found\"")
}

func TestNewResponserErrorNil(t *testing.T) {
	resp := iresponser.NewResponserError(nil)

	assert.Error(t, resp.Error(), "[TestNewResponserErrorNil] Should error")
	assert.Nil(t, resp.Data(), "[TestNewResponserErrorNil] Data should nil")
	assert.Nil(t, resp.Meta(), "[TestNewResponserErrorNil] Meta should nil")
	assert.Empty(t, resp.Message(), "[TestNewResponserErrorNil] Message should empty")
	assert.Equal(t, http.StatusInternalServerError, resp.HTTPStatus(), "[TestNewResponserErrorNil] HTTP Status should 500")
	assert.Empty(t, resp.Code(), "[TestNewResponserErrorNil] Code should empty")
}
